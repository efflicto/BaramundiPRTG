# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import json
import requests
import urllib3
import sys
from requests.auth import HTTPBasicAuth


def main():
    # Get the Parameters from PRTG
    prtg = json.loads(sys.argv[1])
    host = prtg['params'].replace("\\", "")
    username = "{}\\{}".format(prtg['windowslogindomain'], prtg['windowsloginusername'])
    password = prtg['windowsloginpassword']

    # Generate Status count dict
    bms_net_states = {
        -1: "Unknown",
        0: "Assigned",
        1: "Running",
        2: "FinishedSuccess",
        3: "FinishedError",
        4: "FinishedCanceled",
        5: "ReScheduled",
        6: "ReScheduledError",
        7: "WaitingForUser",
        8: "RequirementsNotMet",
        9: "Downloading",
        10: "SkippedDueToIncompatibility",
        11: "NonBlockingWaitingForUser",
    }
    status_counts = {}
    for key, value in bms_net_states.items():
        status_counts[value] = 0

    # Authenticate to the Baramundi server
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)  # Disable the SSL Certificate check
    auth = HTTPBasicAuth(username, password)
    info_url = "{}/bConnect/Info".format(host)
    res = requests.get(url=info_url, auth=auth, verify=False)
    if not res.status_code == 200:
        sys.exit(1)
    # get the job info's
    res = requests.get("{}/bConnect/v1.0/jobinstances.json".format(host), auth=auth,
                       verify=False)
    jobs = res.json()
    for job in jobs:
        status_obj = bms_net_states[int(job['BmsNetState'])]
        status_counts[status_obj] = status_counts[status_obj] + 1

    # get the client info's
    res = requests.get("{}/bConnect/v1.0/endpoints.json".format(host), auth=auth,
                       verify=False)
    endpoints = res.json()
    inactive_clients = 0
    active_clients = 0
    total_clients = len(endpoints)
    for endpoint in endpoints:
        if int(endpoint['Options']) > 1000:
            inactive_clients = inactive_clients + 1
        else:
            active_clients = active_clients + 1

    # compile it into a PRTG readable format
    return_val = {
        "prtg": {
            "result": [
                {
                    "channel": "Total Clients",
                    "float": 0,
                    "value": total_clients
                },
                {
                    "channel": "Active Clients",
                    "float": 0,
                    "value": active_clients
                },
                {
                    "channel": "Inactive Clients",
                    "float": 0,
                    "value": inactive_clients
                },
            ]
        }
    }
    for key, value in status_counts.items():
        return_val['prtg']['result'].append(
            {
                "channel": key,
                "float": 0,
                "value": value
            }
        )
    return json.dumps(return_val)


if __name__ == '__main__':
    print(main())
