#Baramundi PRTG Sensor

Monitors the following channels:

![channels](img/channels.JPG)

## Installation:
Enable bConnect:

![bconnect](img/bconnect.JPG)

Copy `paraprtg.py` to `c:\Program Files (x86)\PRTG Network Monitor\Custom Sensors\python`

Open a terminal on your PRTG server

Go to `c:\Program Files (x86)\PRTG Network Monitor\Python34\Scripts`

Install the required Python modules with `pip3.4 install requests`

Create a new Python sensor

Set the following settings:

![settings](img/sensor_settings.JPG)

The Windows Credentials must be able to connect to the Baramundi bConnect service!

Done.